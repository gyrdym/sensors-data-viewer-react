## Installation
1. Clone/download repo
2. `yarn install` (or `npm install` for npm)

## Usage
**Development**

`yarn run start-dev`

* Build app continuously (HMR enabled)
* App served @ `http://localhost:8080`

**Production**

`yarn run start-prod`

* Build app once (HMR disabled) to `/dist/`
* App served @ `http://localhost:3000`

---

**All commands**

Command | Description
--- | ---
`yarn run start-dev` | Build app continuously (HMR enabled) and serve @ `http://localhost:8080`
`yarn run start-prod` | Build app once (HMR disabled) to `/dist/` and serve @ `http://localhost:3000`
`yarn run build` | Build app to `/dist/`
`yarn run test` | Run tests
`yarn run lint` | Run Typescript linter
`yarn run lint --fix` | Run Typescript linter and fix issues
`yarn run start` | (alias of `yarn run start-dev`)

## A few words about design decisions

- I decided to use React since I'm quite familiar with this library, so it is the best choice to save my time in order to deliver the project on time. 
Beside this, React is a quite performant tool in terms of components rendering, and React has a really huge community which contributes a lot into development of the library.

- I used MaterialUi components in order to not to spend time to develop my own components library. MaterialUi is a powerful lib which provides me with tons of ready to use 
widgets and components of high quality + its components have quite appealing UI and along with that I'm able to use themes app-wise with help of MaterialUi. 

- Currently, I used a Table with pagination instead of virtualized one because of saving my time again, since virtualized table approach is more time-consuming. 
But virtualization is a way more convenient approach in terms of UX, so it would be better to improve the project further and replace the regular table with virtualized one.

- Also, it would be better to add filtering and search functionality, since it makes things easier for users.

- I emulated backend API by introducing implementation of api.getReadings - the records which are going to be loaded sort on the mocked backend side. In the future I would deploy a 
real light-weight node.js server instead of mocked one in order to provide readings from sensors and organize back-end pagination along with client one.

