export const SENSORS_DATA_TABLE_PAGE_SIZES = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
export const DEFAULT_SENSORS_DATA_TABLE_PAGE_SIZE = 50;

export enum SortOrder {
    ASC = 'asc',
    DESC = 'desc',
}
