import {SensorDataRecordFieldType} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.constants';
import {SensorDataRecordField} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.interface';
import {formatDate} from 'components/sensorsDataTable/helpers/formatDate';
import {
    ISensorsDataTableColumn,
    ISensorsDataTableState,
    SensorsDataTableAlignType,
} from 'components/sensorsDataTable/SensorsDataTable.interface';
import {SensorDataTableColumnModel} from 'components/sensorsDataTable/SensorsDataTable.models';
import {DEFAULT_SENSORS_DATA_TABLE_PAGE_SIZE, SortOrder} from 'constants/index';

export const SENSORS_DATA_TABLE_COLUMNS: ISensorsDataTableColumn[] = [
    new SensorDataTableColumnModel({
        id: SensorDataRecordFieldType.Id,
        label: 'ID',
        position: 0,
        minWidth: 100,
    }),
    new SensorDataTableColumnModel({
        align: SensorsDataTableAlignType.Left,
        id: SensorDataRecordFieldType.BoxId,
        label: 'Box id',
        position: 1,
        minWidth: 130,
    }),
    new SensorDataTableColumnModel({
        align: SensorsDataTableAlignType.Center,
        id: SensorDataRecordFieldType.SensorType,
        label: 'Sensor type',
        position: 2,
        minWidth: 130,
        sortable: true,
    }),
    new SensorDataTableColumnModel({
        align: SensorsDataTableAlignType.Center,
        id: SensorDataRecordFieldType.Name,
        label: 'Name',
        position: 3,
        minWidth: 50,
        sortable: true,
    }),
    new SensorDataTableColumnModel({
        align: SensorsDataTableAlignType.Center,
        id: SensorDataRecordFieldType.LowerRange,
        label: 'Lower range',
        position: 4,
        minWidth: 50,
        sortable: true,
    }),
    new SensorDataTableColumnModel({
        align: SensorsDataTableAlignType.Center,
        id: SensorDataRecordFieldType.UpperRange,
        label: 'Upper range',
        position: 5,
        minWidth: 50,
        sortable: true,
    }),
    new SensorDataTableColumnModel({
        align: SensorsDataTableAlignType.Center,
        id: SensorDataRecordFieldType.Longitude,
        label: 'Longitude',
        position: 6,
        minWidth: 150,
        sortable: true,
    }),
    new SensorDataTableColumnModel({
        align: SensorsDataTableAlignType.Center,
        id: SensorDataRecordFieldType.Latitude,
        label: 'Latitude',
        position: 7,
        minWidth: 100,
        sortable: true,
    }),
    new SensorDataTableColumnModel({
        id: SensorDataRecordFieldType.Value,
        label: 'Value',
        position: 8,
        minWidth: 100,
        sortable: true,
    }),
    new SensorDataTableColumnModel({
        id: SensorDataRecordFieldType.Unit,
        label: 'Unit',
        position: 9,
        minWidth: 100,
    }),
    new SensorDataTableColumnModel({
        format: formatDate,
        id: SensorDataRecordFieldType.ReadingDate,
        label: 'Reading date',
        position: 10,
        minWidth: 100,
        sortable: true,
    }),
];

export const DEFAULT_VISIBLE_COLUMN_IDS: SensorDataRecordField[] = [
    SensorDataRecordFieldType.Id,
    SensorDataRecordFieldType.BoxId,
    SensorDataRecordFieldType.Name,
    SensorDataRecordFieldType.Value,
    SensorDataRecordFieldType.Unit,
    SensorDataRecordFieldType.ReadingDate,
];

export const DEFAULT_VISIBLE_COLUMNS = SENSORS_DATA_TABLE_COLUMNS
    .filter((column) => DEFAULT_VISIBLE_COLUMN_IDS.includes(column.id));

export const SENSORS_DATA_TABLE_INITIAL_STATE: ISensorsDataTableState = {
    columns: DEFAULT_VISIBLE_COLUMNS,
    isLoadingInProgress: false,
    pageNumber: 0,
    records: [],
    rowsPerPage: DEFAULT_SENSORS_DATA_TABLE_PAGE_SIZE,
    totalRecordsCount: 0,
    orderBy: SensorDataRecordFieldType.ReadingDate,
    order: SortOrder.ASC,
};
