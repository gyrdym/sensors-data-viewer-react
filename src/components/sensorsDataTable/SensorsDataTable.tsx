import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableContainer from '@material-ui/core/TableContainer';
import {SensorDataRecordField} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.interface';
import {loadReadings} from 'components/sensorsDataTable/helpers/loadReadings';
import {useStyles} from 'components/sensorsDataTable/hooks/useStyles';
import {
    SENSORS_DATA_TABLE_COLUMNS,
    SENSORS_DATA_TABLE_INITIAL_STATE,
} from 'components/sensorsDataTable/SensorsDataTable.constants';
import {ISensorsDataTableState} from 'components/sensorsDataTable/SensorsDataTable.interface';
import SensorsDataTableBody from 'components/sensorsDataTableBody/SensorsDataTableBody';
import SensorsDataTableColumnPicker from 'components/sensorsDataTableColumnPicker/SensorsDataTableColumnPicker';
import SensorsDataTableHead from 'components/sensorsDataTableHead/SensorsDataTableHead';
import SensorsDataTablePagination from 'components/sensorsDataTablePagination/SensorsDataTablePagination';
import {SortOrder} from 'constants/index';
import debounce from 'lodash.debounce';
import * as React from 'react';

const debouncedLoadReadings = debounce(loadReadings, 100);

export default function SensorsDataTable(): JSX.Element {
    const classes = useStyles();
    const [{
        columns,
        records,
        pageNumber,
        rowsPerPage,
        totalRecordsCount,
        orderBy,
        order,
    }, setState] = React.useState<ISensorsDataTableState>(SENSORS_DATA_TABLE_INITIAL_STATE);
    const handleRowsPerPageChange = (event) => {
        setState((state) => ({
            ...state,
            pageNumber: 0,
            rowsPerPage: event.target.value,
        }));
    };
    const handlePageChange = (count, newPageNumber) => {
        setState((state) => ({
            ...state,
            pageNumber: newPageNumber,
        }));
    };
    const onVisibleColumnsChange = (visibleColumns) => {
        setState((state) => ({
            ...state,
            columns: visibleColumns,
        }));
    };
    const createSortHandler = (field: SensorDataRecordField) => () => {
        const isAsc = orderBy === field && order === SortOrder.ASC;

        setState((state) => ({
            ...state,
            order: isAsc
                ? SortOrder.DESC
                : SortOrder.ASC,
            orderBy: field,
        }));
    };
    const renderPagination = () => (
        <SensorsDataTablePagination
            isPrevDisabled={pageNumber === 0}
            isNextDisabled={(pageNumber + 1) * rowsPerPage >= totalRecordsCount}
            recordsPerPage={rowsPerPage}
            recordsOnCurrentPage={records.length}
            pageNumber={pageNumber}
            handleRowsPerPageChange={handleRowsPerPageChange}
            handlePageChange={handlePageChange}
        />
    );

    React.useEffect(() => {
        debouncedLoadReadings(pageNumber, rowsPerPage, orderBy, order, setState);
    }, [pageNumber, rowsPerPage, orderBy, order]);

    return (
        <div>
            <SensorsDataTableColumnPicker
                columns={SENSORS_DATA_TABLE_COLUMNS}
                onVisibleColumnsChange={onVisibleColumnsChange}
            />
            <Paper className={classes.paper}>
                {renderPagination()}
                <TableContainer className={classes.container}>
                    <Table>
                        <SensorsDataTableHead
                            columns={columns}
                            orderBy={orderBy}
                            order={order}
                            createSortHandler={createSortHandler}
                        />
                        <SensorsDataTableBody
                            records={records}
                            columns={columns}
                        />
                    </Table>
                </TableContainer>
                {renderPagination()}
            </Paper>
        </div>
    );
}
