import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles({
    paper: {
        width: '100%',
    },
    container: {
        maxHeight: 800,
    },
});
