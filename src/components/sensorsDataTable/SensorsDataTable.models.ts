import {SensorDataRecordField} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.interface';
import {formatValue} from 'components/sensorsDataTable/helpers/formatValue';
import {
    ISensorsDataTableColumn,
    SensorsDataTableAlignType,
} from 'components/sensorsDataTable/SensorsDataTable.interface';

export class SensorDataTableColumnModel implements ISensorsDataTableColumn {
    public readonly align: SensorsDataTableAlignType;
    public readonly format: (value: any) => string;
    public readonly id: SensorDataRecordField;
    public readonly label: string;
    public readonly minWidth?: string | number;
    public readonly sortable?: boolean;
    public readonly position: number;

    constructor({
        align = SensorsDataTableAlignType.Right,
        format = formatValue,
        id,
        label,
        minWidth,
        sortable = false,
        position,
    }: ISensorsDataTableColumn) {
        this.align = align;
        this.format = format;
        this.id = id;
        this.label = label;
        this.minWidth = minWidth;
        this.sortable = sortable;
        this.position = position;
    }
}
