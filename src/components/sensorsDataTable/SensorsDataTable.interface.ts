import {ISensorDataRecord, SensorDataRecordField} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.interface';
import {SortOrder} from 'constants/index';

export enum SensorsDataTableAlignType {
    Right = 'right',
    Left = 'left',
    Center = 'center',
}

export interface ISensorsDataTableColumn {
    id: SensorDataRecordField;
    label: string;
    position: number;
    minWidth?: string|number;
    align?: SensorsDataTableAlignType;
    format?: (value) => string;
    sortable?: boolean;
}

export interface ISensorsDataTableState {
    columns: ISensorsDataTableColumn[];
    isLoadingInProgress: boolean;
    records: ISensorDataRecord[];
    pageNumber: number;
    rowsPerPage: number;
    totalRecordsCount: number;
    orderBy: SensorDataRecordField;
    order: SortOrder;
}
