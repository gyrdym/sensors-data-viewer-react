import format from 'date-fns/format';

export function formatDate(date: Date): string {
    return format(date, 'dd/MM/yyyy, hh:mm');
}
