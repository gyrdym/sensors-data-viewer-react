import {api} from 'api/index';
import {SensorDataRecordField} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.interface';
import {SortOrder} from 'constants/index';

export function loadReadings(
    pageNumber: number,
    rowsPerPage: number,
    sortBy: SensorDataRecordField,
    sortOrder: SortOrder,
    setState: (value: any) => void,
) {
    api
        .getReadings({
            start: pageNumber * rowsPerPage,
            end: pageNumber * rowsPerPage + rowsPerPage,
            sortBy,
            sortOrder,
        })
        .then(({records: newRecords, totalCount}) => {
            setState((state) => ({
                ...state,
                records: newRecords,
                totalRecordsCount: totalCount,
            }));
        });
}
