export function formatValue(value: any): string {
    if (!value) {
        return '—';
    }

    return String(value || '');
}
