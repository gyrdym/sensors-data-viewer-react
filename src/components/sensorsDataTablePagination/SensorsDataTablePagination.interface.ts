export interface ISensorsDataTablePagination {
    isPrevDisabled: boolean;
    isNextDisabled: boolean;
    recordsPerPage: number;
    recordsOnCurrentPage: number;
    pageNumber: number;
    handleRowsPerPageChange: (event: any) => void;
    handlePageChange: (event: any, page: number) => void;
}
