import TablePagination from '@material-ui/core/TablePagination';
import {ISensorsDataTablePagination} from 'components/sensorsDataTablePagination/SensorsDataTablePagination.interface';
import {SENSORS_DATA_TABLE_PAGE_SIZES} from 'constants/index';
import * as React from 'react';

export default function SensorsDataTablePagination({
    isPrevDisabled,
    isNextDisabled,
    recordsPerPage,
    recordsOnCurrentPage,
    pageNumber,
    handleRowsPerPageChange,
    handlePageChange,
}: ISensorsDataTablePagination): JSX.Element {
    return (
        <TablePagination
            component='div'
            count={-1}
            rowsPerPageOptions={SENSORS_DATA_TABLE_PAGE_SIZES}
            backIconButtonProps={{
                disabled: isPrevDisabled,
            }}
            nextIconButtonProps={{
                disabled: isNextDisabled,
            }}
            rowsPerPage={recordsPerPage}
            labelDisplayedRows={({page}) => `page ${page + 1}, records on page ${recordsOnCurrentPage}`}
            page={pageNumber}
            onChangeRowsPerPage={handleRowsPerPageChange}
            onChangePage={handlePageChange}
        />
    );
}
