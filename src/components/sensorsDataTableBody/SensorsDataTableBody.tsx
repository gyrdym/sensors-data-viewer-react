import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell/TableCell';
import TableRow from '@material-ui/core/TableRow';
import {useStyles} from 'components/sensorsDataTableBody/hooks/useStyles';
import {ISensorsDataTableBodyProps} from 'components/sensorsDataTableBody/SensorsDataTableBody.interface';
import * as React from 'react';

export default function SensorsDataTableBody({
    records = [],
    columns,
}: ISensorsDataTableBodyProps): JSX.Element {
    const classes = useStyles();

    return (
        <TableBody className={classes.root}>
            {records.map((record) => {
                return (
                    <TableRow
                        key={record.hash}
                        hover
                        role="checkbox"
                        tabIndex={-1}
                    >
                        {columns.map((column) => {
                            const value = record[column.id];
                            const formattedValue = column.format?.(value)
                                || String(value || '');

                            return (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                >
                                    {formattedValue}
                                </TableCell>
                            );
                        })}
                    </TableRow>
                );
            })}
        </TableBody>
    );
}
