import {ISensorDataRecord} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.interface';
import {ISensorsDataTableColumn} from 'components/sensorsDataTable/SensorsDataTable.interface';

export interface ISensorsDataTableBodyProps {
    records?: ISensorDataRecord[];
    columns: ISensorsDataTableColumn[];
}
