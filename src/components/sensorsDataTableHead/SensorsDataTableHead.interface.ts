import {SensorDataRecordField} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.interface';
import {ISensorsDataTableColumn} from 'components/sensorsDataTable/SensorsDataTable.interface';
import {SortOrder} from 'constants/index';

export interface ISensorsDataTableHeadProps {
    columns: ISensorsDataTableColumn[];
    orderBy: SensorDataRecordField;
    order: SortOrder;
    createSortHandler: (id: SensorDataRecordField) => () => void;
}
