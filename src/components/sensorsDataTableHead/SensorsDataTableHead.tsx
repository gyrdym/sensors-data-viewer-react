import {TableSortLabel} from '@material-ui/core';
import TableCell from '@material-ui/core/TableCell/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {ISensorsDataTableColumn} from 'components/sensorsDataTable/SensorsDataTable.interface';
import {useStyles} from 'components/sensorsDataTableHead/hooks/useStyles';
import {ISensorsDataTableHeadProps} from 'components/sensorsDataTableHead/SensorsDataTableHead.interface';
import {SortOrder} from 'constants/index';
import * as React from 'react';

export default function SensorsDataTableHead({
    columns,
    orderBy,
    order,
    createSortHandler,
}: ISensorsDataTableHeadProps): JSX.Element {
    const classes = useStyles();
    const renderLabel = (column: ISensorsDataTableColumn) => {
        if (column.sortable) {
            return (
                <TableSortLabel
                    active={orderBy === column.id}
                    direction={orderBy === column.id ? order : SortOrder.ASC}
                    onClick={createSortHandler(column.id)}
                >
                    {column.label}
                </TableSortLabel>
            );
        }

        return column.label;
    };

    return (
        <TableHead>
            <TableRow className={classes.root}>
                {columns.map((column) => (
                    <TableCell
                        className={classes.cell}
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth }}
                    >
                        {renderLabel(column)}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}
