import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles({
    root: {},
    cell: {
        fontWeight: 900,
    },
    visuallyHidden: {},
});
