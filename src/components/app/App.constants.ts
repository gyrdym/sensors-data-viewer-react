import {createMuiTheme} from '@material-ui/core';

export const APP_THEME = createMuiTheme({
    palette: {
        type: 'dark',
    },
});
