import CssBaseline from '@material-ui/core/CssBaseline';
import {ThemeProvider} from '@material-ui/core/styles';
import {APP_THEME} from 'components/app/App.constants';
import {useStyles} from 'components/app/hooks/useStyles';
import SensorsDataTable from 'components/sensorsDataTable/SensorsDataTable';
import * as React from 'react';

export function App(): JSX.Element {
    const classes = useStyles();

    return (
        <ThemeProvider theme={APP_THEME}>
            <CssBaseline />
            <div className={classes.app}>
                <header className={classes.header}>
                    Data from sensors
                </header>
                <SensorsDataTable />
            </div>
        </ThemeProvider>
    );
}
