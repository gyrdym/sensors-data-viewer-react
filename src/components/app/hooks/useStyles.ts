import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles({
    app: {
        boxSizing: 'border-box',
        margin: '0 auto',
        maxWidth: '1200px',
        padding: '40px 20px',
    },
    header: {
        fontSize: 'x-large',
    },
});
