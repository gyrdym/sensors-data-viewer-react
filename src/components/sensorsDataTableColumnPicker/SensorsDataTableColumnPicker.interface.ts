import {ISensorsDataTableColumn} from 'components/sensorsDataTable/SensorsDataTable.interface';

export interface ISensorsDataTableColumnPickerProps {
    columns?: ISensorsDataTableColumn[];
    onVisibleColumnsChange?: (newVisibleColumns: ISensorsDataTableColumn[]) => void;
}
