import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline',
    },
    label: {
        flexBasis: '20%',
        fontWeight: 900,
    },
    select: {
        maxWidth: 400,
    },
}));
