import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel/InputLabel';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select/Select';
import {DEFAULT_VISIBLE_COLUMNS} from 'components/sensorsDataTable/SensorsDataTable.constants';
import {ISensorsDataTableColumn} from 'components/sensorsDataTable/SensorsDataTable.interface';
import {useStyles} from 'components/sensorsDataTableColumnPicker/hooks/useStyles';
import {MENU_PROPS} from 'components/sensorsDataTableColumnPicker/SensorsDataTableColumnPicker.constants';
import {ISensorsDataTableColumnPickerProps} from 'components/sensorsDataTableColumnPicker/SensorsDataTableColumnPicker.interface';
import * as React from 'react';
import {useState} from 'react';

export default function SensorsDataTableColumnPicker({
    columns = [],
    onVisibleColumnsChange = () => undefined,
}: ISensorsDataTableColumnPickerProps = {}): JSX.Element {
    const classes = useStyles();
    const [
        visibleColumns,
        setVisibleColumns,
    ] = useState<ISensorsDataTableColumn[]>(DEFAULT_VISIBLE_COLUMNS);

    const handleChange = (event: React.ChangeEvent<{value: unknown}>) => {
        const newVisibleColumns = (event.target.value as ISensorsDataTableColumn[])
            .sort((column1, column2) => column1.position - column2.position);

        setVisibleColumns(newVisibleColumns);
        onVisibleColumnsChange(newVisibleColumns);
    };

    const renderVisibleColumns = (selected: unknown) => (selected as ISensorsDataTableColumn[])
        .map((column) => column.label)
        .join(', ');

    return (
        <div>
            <FormControl className={classes.formControl}>
                <InputLabel className={classes.label}>
                    Displayed columns
                </InputLabel>
                <Select
                    className={classes.select}
                    multiple
                    value={visibleColumns}
                    onChange={handleChange}
                    input={<Input />}
                    renderValue={renderVisibleColumns}
                    MenuProps={MENU_PROPS}
                >
                    {columns.map((column) => (
                        <MenuItem
                            key={column.id}
                            value={column as any}
                        >
                            <Checkbox checked={visibleColumns.includes(column)} />
                            <ListItemText primary={column.label} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    );
}
