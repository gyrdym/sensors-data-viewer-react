export enum SensorType {
    O3 = 'O3',
    NO2 = 'NO2',
    CO = 'CO',
    Temp = 'TEMP',
    RH = 'RH',
    Unknown = 'UnknownSensorType',
}

export enum UnitType {
    Ppm = 'ppm',
    Percent = '%',
    Celsius = '\u00baC',
    Unknown = 'UnknownUnitType',
}

export interface ISensorDataRecord {
    id: string;
    boxId: string;
    sensorType: SensorType;
    name: string;
    lowerRange: number;
    upperRange: number;
    longitude: number;
    latitude: number;
    value: number;
    unit: UnitType;
    readingDate: Date;
    hash: string;
}

export type SensorDataRecordField = keyof ISensorDataRecord;
