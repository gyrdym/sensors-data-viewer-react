export enum SensorDataRecordFieldType {
    Id = 'id',
    BoxId = 'boxId',
    SensorType = 'sensorType',
    Name = 'name',
    LowerRange = 'lowerRange',
    UpperRange = 'upperRange',
    Longitude = 'longitude',
    Latitude = 'latitude',
    Value = 'value',
    Unit = 'unit',
    ReadingDate = 'readingDate',
}
