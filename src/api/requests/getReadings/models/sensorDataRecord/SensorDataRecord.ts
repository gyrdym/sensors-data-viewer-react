import {
    ISensorDataRecord,
    SensorType,
    UnitType,
} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.interface';

interface ISensorDataRecordConfig {
    boxId: string;
    id: string;
    latitude: number;
    longitude: number;
    lowerRange: number;
    name: string;
    value: number;
    readingDate: Date;
    sensorType: SensorType;
    unit: UnitType;
    upperRange: number;
}

export class SensorDataRecord implements ISensorDataRecord {
    public readonly boxId: string;
    public readonly id: string;
    public readonly latitude: number;
    public readonly longitude: number;
    public readonly lowerRange: number;
    public readonly name: string;
    public readonly value: number;
    public readonly readingDate: Date;
    public readonly sensorType: SensorType;
    public readonly unit: UnitType;
    public readonly upperRange: number;

    constructor({
       boxId,
       id,
       latitude,
       longitude,
       lowerRange,
       name,
       value,
       readingDate,
       sensorType,
       unit,
       upperRange,
    }: ISensorDataRecordConfig) {
        this.boxId = boxId;
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.lowerRange = lowerRange;
        this.name = name;
        this.value = value;
        this.readingDate = readingDate;
        this.sensorType = sensorType;
        this.unit = unit;
        this.upperRange = upperRange;
    }

    get hash(): string {
        return `${this.id}:${this.boxId}:${this.readingDate.getTime()}`;
    }
}
