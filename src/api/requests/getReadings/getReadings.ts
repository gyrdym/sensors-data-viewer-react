import data from 'api/requests/getReadings/data/sensor_readings.json';
import {GET_READINGS_REQUEST_SORTERS} from 'api/requests/getReadings/getReadings.constants';
import {IGetReadingsParams} from 'api/requests/getReadings/getReadings.interface';
import {parseSensorDataRecord} from 'api/requests/getReadings/helpers/parsing/parseSensorDataRecord';

export async function getReadings({
    start,
    end,
    sortBy,
    sortOrder,
}: IGetReadingsParams) {
    const records = (await Promise.resolve(data))
        .map(parseSensorDataRecord)
        .sort((record1, record2) => GET_READINGS_REQUEST_SORTERS[sortBy](
            record1[sortBy],
            record2[sortBy],
            sortOrder,
        ))
        .slice(start, end);

    return {
        records,
        totalCount: data.length,
    };
}
