import {sortDates} from 'api/requests/getReadings/helpers/sorting/sortDates';
import {sortNumbers} from 'api/requests/getReadings/helpers/sorting/sortNumbers';
import {sortStrings} from 'api/requests/getReadings/helpers/sorting/sortStrings';
import {SensorDataRecordFieldType} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.constants';
import {SortOrder} from 'constants/index';

export const GET_READINGS_REQUEST_SORTERS: {[key: string]: (value1, value2, sortOrder: SortOrder) => number} = {
    [SensorDataRecordFieldType.SensorType]: sortStrings,
    [SensorDataRecordFieldType.Name]: sortStrings,
    [SensorDataRecordFieldType.LowerRange]: sortNumbers,
    [SensorDataRecordFieldType.UpperRange]: sortNumbers,
    [SensorDataRecordFieldType.Longitude]: sortNumbers,
    [SensorDataRecordFieldType.Latitude]: sortNumbers,
    [SensorDataRecordFieldType.Value]: sortNumbers,
    [SensorDataRecordFieldType.ReadingDate]: sortDates,
};
