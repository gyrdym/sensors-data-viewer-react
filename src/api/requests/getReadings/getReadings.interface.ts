import {SensorDataRecordField} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.interface';
import {SortOrder} from 'constants/index';

export interface IGetReadingsParams {
    start: number;
    end: number;
    sortBy: SensorDataRecordField;
    sortOrder: SortOrder;
}
