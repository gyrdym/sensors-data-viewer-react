import {SortOrder} from 'constants/index';

export function sortNumbers(value1: number, value2: number, sortOrder: SortOrder): number {
    if (sortOrder === SortOrder.DESC) {
        return value2 - value1;
    }

    return value1 - value2;
}
