import {SortOrder} from 'constants/index';

export function sortStrings(value1: string, value2: string, sortOrder: SortOrder): number {
    if (sortOrder === SortOrder.DESC) {
        return value2.localeCompare(value1);
    }

    return value1.localeCompare(value2);
}
