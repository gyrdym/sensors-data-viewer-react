import {SortOrder} from 'constants/index';

export function sortDates(value1: Date, value2: Date, sortOrder: SortOrder): number {
    if (sortOrder === SortOrder.DESC) {
        return value2.getTime() - value1.getTime();
    }

    return value1.getTime() - value2.getTime();
}
