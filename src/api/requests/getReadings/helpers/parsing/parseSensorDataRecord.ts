import {parseSensorType} from 'api/requests/getReadings/helpers/parsing/parseSensorType';
import {parseUnit} from 'api/requests/getReadings/helpers/parsing/parseUnit';
import {SensorDataRecord} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord';
import {ISensorDataRecordUnparsed} from 'api/requests/getReadings/models/sensorDataRecordUnparsed/sensorDataRecordUnparsed.interface';

export function parseSensorDataRecord(unparsedRecord: ISensorDataRecordUnparsed) {
    return new SensorDataRecord({
        boxId: unparsedRecord.box_id,
        id: unparsedRecord.id,
        latitude: unparsedRecord.latitude,
        longitude: unparsedRecord.longitude,
        lowerRange: unparsedRecord.range_l,
        name: unparsedRecord.name,
        readingDate: new Date(unparsedRecord.reading_ts),
        sensorType: parseSensorType(unparsedRecord.sensor_type),
        unit: parseUnit(unparsedRecord.unit),
        upperRange: unparsedRecord.range_u,
        value: unparsedRecord.reading,
    });
}
