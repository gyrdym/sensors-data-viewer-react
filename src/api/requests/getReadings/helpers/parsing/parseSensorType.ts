import {SensorType} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.interface';

export function parseSensorType(unparsedSensorType: string): SensorType {
    switch (unparsedSensorType) {
        case 'O3':
            return SensorType.O3;

        case 'CO':
            return SensorType.CO;

        case 'NO2':
            return SensorType.NO2;

        case 'RH':
            return SensorType.RH;

        case 'TEMP':
            return SensorType.Temp;

        default:
            // tslint:disable-next-line:no-console
            console.error('Unknown sensor type: ', unparsedSensorType);

            return SensorType.Unknown;
    }
}
