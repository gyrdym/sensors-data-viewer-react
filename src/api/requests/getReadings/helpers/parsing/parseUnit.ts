import {UnitType} from 'api/requests/getReadings/models/sensorDataRecord/SensorDataRecord.interface';

export function parseUnit(unparsedUnitType: string): UnitType {
    switch (unparsedUnitType) {
        case 'ppm':
            return UnitType.Ppm;

        case '%':
            return UnitType.Percent;

        case '\u00baC':
            return UnitType.Celsius;

        default:
            // tslint:disable-next-line:no-console
            console.error('Unknown unit type: ', unparsedUnitType);

            return UnitType.Unknown;
    }
}
