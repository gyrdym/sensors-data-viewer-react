import {getReadings} from 'api/requests/getReadings/getReadings';

export const api = {
    getReadings,
};
